package fr.umfds.tp4;

import java.util.ArrayList;

public class Courriel {
	private String mail;
	private String titre;
	private String message;
	private ArrayList<String> pieceJointe;
	
	public Courriel(String m, String t,String mess,ArrayList<String> pj) {
		mail = m;
		titre = t;
		message = mess;
		pieceJointe = pj;
	}
	
	public Courriel(String m, String t,String mess) {
		mail = m;
		titre = t;
		message = mess;
		pieceJointe = null;
	}
	
	public boolean envoyer() {
		if(titre.isEmpty()) {
			System.out.println("1");
			return false;
		}
		if(!mail.matches("(.+)@(.+)\\.(.+)")) {
			System.out.println("2");
			return false;
		}
		if((message.contains("PJ")||message.contains("joint")||message.contains("jointe")) && !(pieceJointe.size()==0)) {
			return false;
		}
		return true;
	}
}
